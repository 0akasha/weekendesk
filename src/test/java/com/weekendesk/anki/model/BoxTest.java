package com.weekendesk.anki.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class BoxTest {

    private Box box;

    @Before
    public void setUp() {
        box = new Box();
    }

    @Test
    public void createBoxTest() {
        assertNotNull(box.getCards());
    }

    @Test
    public void boxHasCardsTest() {
        assertFalse(box.hasCards());
        List<Card> cards = new ArrayList<>();
        cards.add(new Card("Q1","A1"));
        box.setCards(cards);
        assertTrue(box.hasCards());
    }

    @Test
    public void addACardToBoxTest() {
        box.addCard(new Card("Q1","A1"));
        assertEquals(box.getCards().size(),1);
    }

}
