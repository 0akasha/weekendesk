package com.weekendesk.anki.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class GameTest {

    private static Game game;
    private static final Card oneCard = new Card("Q1", "A1");
    private static final Card twoCard = new Card("Q2","A2");
    private List<Card> cards;

    @Before
    public void setUp() {
        game = new Game();
        cards = new ArrayList<>();
    }

    @Test
    public void hasThreeBoxes() {
        assertNotNull(game.getRedBox());
        assertNotNull(game.getOrangeBox());
        assertNotNull(game.getGreenBox());
    }

    @Test
    public void hasADeckOfCards() {
        cards = Arrays.asList(oneCard, twoCard);
        game.setDeck(cards);
        assertNotNull(game.getDeck());
    }

    @Test
    public void checkGameIsOver() {
        cards.add(oneCard);
        game.getOrangeBox().setCards(cards);
        assertFalse(game.isOver());
        game.getOrangeBox().setCards(new ArrayList<>());
        game.getGreenBox().setCards(cards);
        assertTrue(game.isOver());
    }

    @Test
    public void checkDayIsOver() {
        cards.add(oneCard);
        game.getRedBox().setCards(cards);
        game.getOrangeBox().setCards(cards);
        assertFalse(game.isDayOver());
        game.getRedBox().setCards(new ArrayList<>());
        assertTrue(game.isDayOver());
    }

}
