package com.weekendesk.anki.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CardTest {

    @Test
    public void cardWithQuestionAndAnswer() {
        Card card = new Card("Question", "Answer");
        assertNotNull(card);
        assertEquals(card.getQuestion(), "Question");
        assertEquals(card.getAnswer(), "Answer");
    }
}
