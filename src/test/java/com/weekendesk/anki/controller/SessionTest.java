package com.weekendesk.anki.controller;

import com.weekendesk.anki.model.Box;
import com.weekendesk.anki.model.Card;
import com.weekendesk.anki.model.Game;
import com.weekendesk.anki.service.BoxService;
import com.weekendesk.anki.service.CardService;
import com.weekendesk.anki.service.FileService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


public class SessionTest {

    @Mock
    private Game gameMock;
    @Mock
    private FileService fileServiceMock;
    @Mock
    private BoxService boxServiceMock;
    @Mock
    private CardService cardServiceMock;

    private Session session;
    private static final Card oneCard = new Card("Q1","A1");

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        session = new Session(fileServiceMock, boxServiceMock, cardServiceMock, gameMock);
    }

    @Test
    public void initGameTest() throws IOException {
        String fileName = "src/test/resources/FileWithHeader";
        List<Card> deck = new ArrayList<>();
        deck.add(oneCard);
        when(fileServiceMock.readCards(any(FileReader.class))).thenReturn(deck);
        session.initGame(fileName);
        verify(gameMock, times(1)).setDeck(deck);
    }

    @Test(expected = FileNotFoundException.class)
    public void wrongFileTest() throws IOException {
        String fileName = "src/test/resources/error";
        session.initGame(fileName);
    }

    @Test
    public void isThereAnyCardToStudyTest() {
        List<Card> deck = new ArrayList<>();
        deck.add(oneCard);
        when(gameMock.getDeck()).thenReturn(deck);
        assertTrue(session.isThereAnyCardToStudy());
        when(gameMock.getDeck()).thenReturn(new ArrayList<>());
        assertFalse(session.isThereAnyCardToStudy());
    }

    @Test
    public void dayIsOverTest() {
        Box greenBox = new Box();
        Box orangeBox = new Box();
        Box redBox = new Box();
        when(gameMock.getRedBox()).thenReturn(redBox);
        when(gameMock.getOrangeBox()).thenReturn(orangeBox);
        when(gameMock.getGreenBox()).thenReturn(greenBox);
        doNothing().when(boxServiceMock).moveBoxToAnotherBox(orangeBox, redBox);
        doNothing().when(boxServiceMock).moveBoxToAnotherBox(greenBox, orangeBox);
        session.dayIsOver();
        verify(boxServiceMock, times(1)).moveBoxToAnotherBox(orangeBox, redBox);
        verify(boxServiceMock, times(1)).moveBoxToAnotherBox(greenBox, orangeBox);
    }

    @Test
    public void moveCardToRedBoxTest() {
        Box redBox = new Box();
        Box orangeBox = new Box();
        Box greenBox = new Box();
        when(gameMock.getRedBox()).thenReturn(redBox);
        when(gameMock.getOrangeBox()).thenReturn(orangeBox);
        when(gameMock.getGreenBox()).thenReturn(greenBox);
        doNothing().when(boxServiceMock).moveCardToBox(redBox, oneCard);
        doNothing().when(boxServiceMock).moveCardToBox(orangeBox, oneCard);
        doNothing().when(boxServiceMock).moveCardToBox(greenBox, oneCard);
        session.moveCardToBox(oneCard, "R");
        verify(boxServiceMock, times(1)).moveCardToBox(redBox, oneCard);
        verify(boxServiceMock, never()).moveCardToBox(orangeBox, oneCard);
        verify(boxServiceMock, never()).moveCardToBox(greenBox, oneCard);
    }

    @Test
    public void moveCardToOrangeBoxTest() {
        Box redBox = new Box();
        Box orangeBox = new Box();
        Box greenBox = new Box();
        when(gameMock.getRedBox()).thenReturn(redBox);
        when(gameMock.getOrangeBox()).thenReturn(orangeBox);
        when(gameMock.getGreenBox()).thenReturn(greenBox);
        doNothing().when(boxServiceMock).moveCardToBox(redBox, oneCard);
        doNothing().when(boxServiceMock).moveCardToBox(orangeBox, oneCard);
        doNothing().when(boxServiceMock).moveCardToBox(greenBox, oneCard);
        session.moveCardToBox(oneCard, "O");
        verify(boxServiceMock, never()).moveCardToBox(redBox, oneCard);
        verify(boxServiceMock, times(1)).moveCardToBox(orangeBox, oneCard);
        verify(boxServiceMock, never()).moveCardToBox(greenBox, oneCard);
    }

    @Test
    public void moveCardToGreenBoxTest() {
        Box redBox = new Box();
        Box orangeBox = new Box();
        Box greenBox = new Box();
        when(gameMock.getRedBox()).thenReturn(redBox);
        when(gameMock.getOrangeBox()).thenReturn(orangeBox);
        when(gameMock.getGreenBox()).thenReturn(greenBox);
        doNothing().when(boxServiceMock).moveCardToBox(redBox, oneCard);
        doNothing().when(boxServiceMock).moveCardToBox(orangeBox, oneCard);
        doNothing().when(boxServiceMock).moveCardToBox(greenBox, oneCard);
        session.moveCardToBox(oneCard, "G");
        verify(boxServiceMock, never()).moveCardToBox(redBox, oneCard);
        verify(boxServiceMock, never()).moveCardToBox(orangeBox, oneCard);
        verify(boxServiceMock, times(1)).moveCardToBox(greenBox, oneCard);
    }

    @Test
    public void prepareToStudyTest() {
        Box redBox = new Box();
        List<Card> cards = new ArrayList<>();
        cards.add(oneCard);
        redBox.setCards(cards);
        List<Card> deck = new ArrayList<>();
        when(gameMock.getRedBox()).thenReturn(redBox);
        when(gameMock.getDeck()).thenReturn(deck);
        doNothing().when(boxServiceMock).moveBoxToDeck(redBox, deck);
        session.prepareToStudy();
        verify(boxServiceMock, times(1)).moveBoxToDeck(redBox, deck);
    }
}
