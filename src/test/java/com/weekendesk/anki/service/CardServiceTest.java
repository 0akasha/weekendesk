package com.weekendesk.anki.service;

import com.weekendesk.anki.model.Card;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CardServiceTest {

    private CardService cardService;
    private static Card oneCard = new Card("Q1", "A1");
    private static Card twoCard = new Card("Q2","A2");

    @Before
    public void setUp() {
        cardService = new CardService();
    }

    @Test
    public void readFirstCardFromDeckTest() {
        List<Card> cards = new ArrayList<>();
        cards.add(oneCard);
        cards.add(twoCard);
        Card cardFromDeck = cardService.readFirstCardFromDeck(cards);
        assertEquals(cardFromDeck.getQuestion(), oneCard.getQuestion());
        assertEquals(cardFromDeck.getAnswer(), oneCard.getAnswer());
        assertEquals(cards.size(),1);
    }
}
