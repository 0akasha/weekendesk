package com.weekendesk.anki.service;

import com.weekendesk.anki.model.Card;
import org.junit.Before;
import org.junit.Test;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FileServiceTest {

    private FileReader file;
    private FileService fileService;
    private Card cardExpected;

    @Before
    public void setUp() {
        fileService = new FileService();
        cardExpected = new Card("What enzyme breaks down sugars mouth and digestive tract?", "Amylase");
    }

    @Test
    public void testReadCardsReturnsListCardsTest() throws IOException {
        file = new FileReader("src/test/resources/FileWithoutHeader");
        validateCards(fileService.readCards(file));
    }

    @Test
    public void testReadCardsReturnsListCardsWithNoHeadersTest() throws IOException {
        file = new FileReader("src/test/resources/FileWithHeader");
        validateCards(fileService.readCards(file));
    }

    private void validateCards(List<Card> cards) {
        assertEquals(1, cards.size());
        assertEquals(cardExpected.getQuestion(), cards.get(0).getQuestion());
        assertEquals(cardExpected.getAnswer(), cards.get(0).getAnswer());
    }
}
