package com.weekendesk.anki.service;

import com.weekendesk.anki.model.Card;
import com.weekendesk.anki.model.Game;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class BoxServiceTest {

    private BoxService boxService;
    private static Game game;

    @Before
    public void setUp() {
        boxService = new BoxService();
        game = new Game();
        game.getRedBox().addCard(new Card("Q1","A1"));
    }

    @Test
    public void moveBoxRedToDeckTest() {
        boxService.moveBoxToDeck(game.getRedBox(), game.getDeck());
        assertFalse(game.getRedBox().hasCards());
        assertEquals(game.getDeck().size(),1);
    }

    @Test
    public void moveBoxToBoxTest() {
        boxService.moveBoxToAnotherBox(game.getRedBox(), game.getOrangeBox());
        assertFalse(game.getRedBox().hasCards());
        assertEquals(game.getOrangeBox().getCards().size(),1);
    }

    @Test
    public void moveCardToBoxTest() {
        Card two = new Card("Q2","A2");
        boxService.moveCardToBox(game.getRedBox(),two);
        assertEquals(game.getRedBox().getCards().size(),2);
    }
}
