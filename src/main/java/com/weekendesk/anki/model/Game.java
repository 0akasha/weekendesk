package com.weekendesk.anki.model;

import java.util.ArrayList;
import java.util.List;

public class Game {

    private Box redBox;
    private Box orangeBox;
    private Box greenBox;

    private List<Card> deck;

    public Game () {
        redBox = new Box();
        orangeBox = new Box();
        greenBox = new Box();
        deck = new ArrayList<>();
    }

    public Box getRedBox() {
        return redBox;
    }

    public void setRedBox(Box redBox) {
        this.redBox = redBox;
    }

    public Box getOrangeBox() {
        return orangeBox;
    }

    public void setOrangeBox(Box orangeBox) {
        this.orangeBox = orangeBox;
    }

    public Box getGreenBox() {
        return greenBox;
    }

    public void setGreenBox(Box greenBox) {
        this.greenBox = greenBox;
    }

    public List<Card> getDeck() {
        return deck;
    }

    public void setDeck(List<Card> deck) {
        this.deck = deck;
    }

    public boolean isOver() {
        return (deck.isEmpty() && !redBox.hasCards() && !orangeBox.hasCards());
    }

    public boolean isDayOver() {
        return !redBox.hasCards();
    }
}
