package com.weekendesk.anki.model;

import java.util.ArrayList;
import java.util.List;

public class Box {

    private List<Card> cards;

    public Box() {
        this.cards = new ArrayList<>();
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public boolean hasCards() {
        return !this.cards.isEmpty();
    }

    public void addCard(Card card) {
        this.cards.add(card);
    }

}
