package com.weekendesk.anki.controller;

import com.weekendesk.anki.model.Card;
import com.weekendesk.anki.model.Game;
import com.weekendesk.anki.service.CardService;
import com.weekendesk.anki.service.FileService;
import com.weekendesk.anki.service.BoxService;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

public class Session {

    private Game game;
    private FileService fileService;
    private BoxService boxService;
    private CardService cardService;

    public Session(FileService fileService, BoxService boxService, CardService cardService, Game game) {
        this.fileService = fileService;
        this.boxService = boxService;
        this.cardService = cardService;
        this.game = game;
    }

    public void initGame(String fileName) throws FileNotFoundException {
        List<Card> deck = fileService.readCards(new FileReader(fileName));
        game.setDeck(deck);
    }

    public boolean gameIsOver() {
        return game.isOver();
    }

    public boolean isDayOver() {
        return game.isDayOver();
    }

    public boolean isThereAnyCardToStudy() {
        return !game.getDeck().isEmpty();
    }

    public void dayIsOver() {
        boxService.moveBoxToAnotherBox(game.getOrangeBox(), game.getRedBox());
        boxService.moveBoxToAnotherBox(game.getGreenBox(), game.getOrangeBox());
    }

    public Card studyACard() {
        return cardService.readFirstCardFromDeck(game.getDeck());
    }

    public void moveCardToBox(Card card, String boxToStore) {
        switch (boxToStore) {
            case "R":
                boxService.moveCardToBox(game.getRedBox(), card);
                break;
            case "O":
                boxService.moveCardToBox(game.getOrangeBox(), card);
                break;
            case "G":
                boxService.moveCardToBox(game.getGreenBox(), card);
        }
    }

    public void prepareToStudy() {
        if(game.getDeck().isEmpty()) {
            boxService.moveBoxToDeck(game.getRedBox(), game.getDeck());
        }
    }
}
