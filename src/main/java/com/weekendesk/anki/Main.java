package com.weekendesk.anki;

import com.weekendesk.anki.controller.Session;
import com.weekendesk.anki.model.Card;
import com.weekendesk.anki.model.Game;
import com.weekendesk.anki.service.BoxService;
import com.weekendesk.anki.service.CardService;
import com.weekendesk.anki.service.FileService;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public final class Main {

    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    private static Session session = new Session(new FileService(), new BoxService(), new CardService(), new Game());

    public static void main (String... args) {
        try {
            System.out.println("Add the file name with the cards: ");
            String filename = bufferedReader.readLine();
            session.initGame(filename);
            while (!session.gameIsOver()) {
                System.out.println("############################################");
                System.out.println("##       Welcome to a new session!");
                System.out.println("############################################");
                do {
                    studySession();
                } while (!session.isDayOver());
                if (session.gameIsOver()) break;
                session.dayIsOver();
                System.out.println("The session for today is over, Goodbye!!");
            }
            System.out.println("Congratulations! You know all the answers");
        } catch (FileNotFoundException e) {
            System.out.println("The file doesn't exist");
        } catch (IOException e) {
            System.out.println("An error occurred.");
        }
    }

    private static void studySession() throws IOException {
        session.prepareToStudy();
        while(session.isThereAnyCardToStudy()) {
            Card card = session.studyACard();
            System.out.println("Query:" + card.getQuestion());
            System.out.println("Read the answer?[Y]");
            while(!bufferedReader.readLine().equals("Y"));
            System.out.println("Answer:" + card.getAnswer());
            System.out.println("Which box do you want to put the card?[R|O|G]?");
            String boxToStore;
            do {
                boxToStore = bufferedReader.readLine();
            } while(!(boxToStore.equalsIgnoreCase("R")
                    || boxToStore.equalsIgnoreCase("O")
                    || boxToStore.equalsIgnoreCase("G")));
            session.moveCardToBox(card, boxToStore);
        }
    }
}
