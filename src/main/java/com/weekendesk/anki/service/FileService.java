package com.weekendesk.anki.service;

import com.weekendesk.anki.model.Card;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class FileService {

    public List<Card> readCards(FileReader cardFile) {
        BufferedReader bufferedReader = new BufferedReader(cardFile);
        return bufferedReader.lines()
                .map(line -> {
                    String[] lineSplit = line.split(Pattern.quote("|"));
                    return new Card(lineSplit[0], lineSplit[1]);
                })
                .filter(card -> !card.getAnswer().equals("card answer") && !card.getQuestion().equals("card question"))
                .collect(Collectors.toList());
    }
}
