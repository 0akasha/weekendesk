package com.weekendesk.anki.service;

import com.weekendesk.anki.model.Box;
import com.weekendesk.anki.model.Card;

import java.util.ArrayList;
import java.util.List;

public class BoxService {

    public void moveBoxToDeck(Box box, List<Card> deck) {
        deck.clear();
        deck.addAll(box.getCards());
        box.setCards(new ArrayList<>());
    }

    public void moveBoxToAnotherBox(Box sourceBox, Box finalBox) {
        finalBox.setCards(sourceBox.getCards());
        sourceBox.setCards(new ArrayList<>());
    }

    public void moveCardToBox(Box box, Card card) {
        box.addCard(card);
    }

}
