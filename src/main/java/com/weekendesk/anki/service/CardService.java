package com.weekendesk.anki.service;

import com.weekendesk.anki.model.Card;

import java.util.List;

public class CardService {

    public Card readFirstCardFromDeck(List<Card> cards) {
        return cards.remove(0);
    }
}
